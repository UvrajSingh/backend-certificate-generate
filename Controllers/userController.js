const User = require("../Model/user");

const addUserDetail = async(req,res)=>{
    try {
        let {name ,email ,phone ,registerId ,certificateTitle,dob,gender,bloodGroup,city,place,state } = req.body
         const result = await User.create(req.body); 
         if(!result){
            res.status(400).json("something went wrong")
         }
         res.status(200).json({
            message:"successfully created data",
            data:result
         })
    } catch(error) {
        res.status(500).json({
            message:"something went wrong",
            error:error
        })
    }
}

module.exports = {addUserDetail}
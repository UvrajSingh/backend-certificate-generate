const mongoose = require('mongoose');
const express =require('express');

const userSchema = mongoose.Schema({
  name:{
    type: String, 
    require: true
  },
  email:{
    type: String, 
    require: true
  },  
  phone:{
    type: String, 
    require: true
  },  
  registerId:{
    type: String, 
    require: true
  },  
  certificateTitle:{
    type: String, 
    require: true
  },  
  dob:{
    type: String, 
    require: true
  },  
  gender:{
    type: String, 
    require: true
  },  
  bloodGroup:{
    type: String, 
    require: true
  },  
  city:{
    type: String, 
    require: true
  },  
  place:{
    type: String, 
    require: true
  },  
  state:{
    type: String, 
    require: true
  }
})

const User = mongoose.model("user" ,userSchema);

module.exports = User

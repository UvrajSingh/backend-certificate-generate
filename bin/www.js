const express = require("express");
const app = require('../app')
const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config();


let port = process.env.PORT || 3002;
let dbUrl = process.env.DBURL || "mongodb://localhost:27017/certificate-db";

mongoose
  .connect(dbUrl, {})
  .then(() => console.log("Connected to MongoDB"))
  .catch((error) => console.error("Connection error", error));


  app.listen(port, function () {
    console.log(`Started application on port, ${port}`);
  });

  